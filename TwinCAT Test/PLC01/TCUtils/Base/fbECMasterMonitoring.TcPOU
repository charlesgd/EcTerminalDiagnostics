﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.12">
  <POU Name="fbECMasterMonitoring" Id="{9508ef76-585a-4691-8047-32b53eca5605}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK fbECMasterMonitoring
VAR_INPUT
	isRedundant : BOOL;
	bResetMaster : BOOL;
	bRescan : BOOL;
END_VAR
VAR_OUTPUT
END_VAR
VAR
	eState : eECMasterMon_States := eECMasterMon_States.Init;
	ePrevState : eECMasterMon_States := eECMasterMon_States.Init;

	sAmsNetId AT %I* : AMSNETID;
	nCfgSlaveCount AT %I* : UINT;
	nSlaveCount AT %I* : UINT;
	nSlaveCount2 AT %I* : UINT;	

	// Slave Database
	asEcSlaveAddr : ARRAY[0..ECMASTER_MAX_SLAVES] OF UINT;
	asEcSlaveTopology : ARRAY[0..ECMASTER_MAX_SLAVES] OF ST_TopologyDataEx;
	asEcSlaveStates : ARRAY[0..ECMASTER_MAX_SLAVES] OF ST_EcSlaveState;
	asEcSlaveIdentities : ARRAY[0..ECMASTER_MAX_SLAVES] OF ST_EcSlaveIdentity;
	asEcScannedSlaves : ARRAY[0..ECMASTER_MAX_SLAVES] OF ST_EcSlaveScannedData;
	asEtherCATSlaves : ARRAY[0..ECMASTER_MAX_SLAVES] OF IEtherCATSlave;
	
	// Get Slave Address
	fbSlaveAddr : FB_EcGetAllSlaveAddr;
	
	// Get Slave Topology
	fbSlaveTopology : FB_EcGetSlaveTopologyInfo;
	
	// Get Configured Slaves
	fbSlaveConfig : FB_EcGetConfSlaves;
	
	// Get Slave State
	fbSlaveState : FB_EcGetAllSlaveStates;
	nSlavesRead : UINT;
	
	// Get Slave Identity
	fbSlaveIdentity : FB_EcGetSlaveIdentity;
	nSlaveIndex_Identity : UINT;
	
	// Scan Slaves
	fbScanSlaves : FB_EcGetScannedSlaves;
	nScannedSlaves : UDINT;
	
	rtResetMaster : R_TRIG;
	rtRescan : R_TRIG;
	
	// Diag FBs
	nSlaveIndex_Diagnostics : UINT;
	nParameterIndex_Diagnostics : UINT;
	fbCoERead : FB_EcCoESdoRead;
END_VAR
VAR CONSTANT
	ECMASTER_MAX_SLAVES : UINT := 50;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[rtResetMaster(CLK:=bResetMaster);
rtRescan(CLK:=bRescan);

ePrevState := eState;

CASE eState OF
	eECMasterMon_States.Init:
		fbSlaveAddr.sNetId := F_CreateAmsNetId(nIds := sAmsNetId);
		fbSlaveAddr.pAddrBuf := ADR(asEcSlaveAddr);
		fbSlaveAddr.cbBufLen := ECMASTER_MAX_SLAVES * SIZEOF(UINT);
	
		fbSlaveTopology.sNetId := F_CreateAmsNetId(nIds := sAmsNetId);
		fbSlaveTopology.pAddrBuf := ADR(asEcSlaveTopology);
		fbSlaveTopology.cbBufLen := ECMASTER_MAX_SLAVES * SIZEOF(ST_TopologyDataEx);	
	
		fbSlaveState.sNetId := F_CreateAmsNetId(nIds:= sAmsNetId);
		fbSlaveState.pStateBuf := ADR(asEcSlaveStates);
		fbSlaveState.cbBufLen := ECMASTER_MAX_SLAVES * SIZEOF(ST_EcSlaveState);
	
		fbSlaveIdentity.sNetId := F_CreateAmsNetId(nIds:= sAmsNetId);
	
		fbScanSlaves.sNetId := F_CreateAmsNetId(nIds := sAmsNetId);
		fbScanSlaves.pArrEcScannedSlaveInfo := ADR(asEcScannedSlaves);
		fbScanSlaves.cbBufLen := ECMASTER_MAX_SLAVES * SIZEOF(ST_EcSlaveScannedData);
		
		fbCoERead.sNetId := F_CreateAmsNetId(nIds := sAmsNetId);
		
		//eState := eECMasterMon_States.GetAddresses;
		eState := eECMasterMon_States.GetTopology;
		
	eECMasterMon_States.GetAddresses:
		IF GetSlaveAddresses() THEN
			eState :=  eECMasterMon_States.GetTopology;
		END_IF
	eECMasterMon_States.GetTopology:
		IF GetSlaveTopology() THEN
			eState := eECMasterMon_States.GetIdentities;
		END_IF
	eECMasterMon_States.GetIdentities:
		IF GetSlaveIdentities() THEN
			SortTopology();
			eState := eECMasterMon_States.Scan;
		END_IF
	eECMasterMon_States.Scan:
		IF ScanSlaves() THEN
			eState := eECMasterMon_States.Monitor;
		END_IF
	eECMasterMon_States.Monitor:
		GetSlavesStates();
		GetSlaveDiagnostics(nSlaveIndex_Diagnostics, nParameterIndex_Diagnostics);
		IF rtRescan.Q THEN
			eState := eECMasterMon_States.Scan;
		END_IF
END_CASE

IF eState <> ePrevState  THEN
	CASE eState OF
		eECMasterMon_States.GetIdentities:
			nSlaveIndex_Identity := 0;
		eECMasterMon_States.Monitor:
			nSlaveIndex_Diagnostics := 0;
	END_CASE
END_IF
]]></ST>
    </Implementation>
    <Property Name="bScanned" Id="{ce606337-bf96-4704-9d29-b51f3c6450cb}">
      <Declaration><![CDATA[PROPERTY PUBLIC bScanned : BOOL]]></Declaration>
      <Get Name="Get" Id="{a87b8ec3-9f06-494b-a70d-174a1bc75d38}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[bScanned := (eState > eECMasterMon_States.Scan);]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Method Name="CheckScannedSlaves" Id="{03c9781a-5741-4ca2-a97c-57ded66e929a}">
      <Declaration><![CDATA[METHOD PUBLIC CheckScannedSlaves : BOOL
VAR_INPUT
	pMismatchSlaveIndexes : POINTER TO ARRAY[0..EC_MAX_SLAVES] OF UINT;
	nMaxSlaveResults : UDINT;
END_VAR
VAR
	nIndex : UDINT;
	nCurrentResult : UDINT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Clear Array
FOR nCurrentResult := 0 TO nMaxSlaveResults DO
	pMismatchSlaveIndexes^[nIndex] := 0;
END_FOR

nCurrentResult := 0;

FOR nIndex := 0 TO nCfgSlaveCount DO
	IF asEcSlaveIdentities[nIndex].vendorId <> asEcScannedSlaves[nIndex].stSlaveIdentity.vendorId OR asEcSlaveIdentities[nIndex].productCode <> asEcScannedSlaves[nIndex].stSlaveIdentity.productCode THEN
		pMismatchSlaveIndexes^[nCurrentResult] := asEcSlaveAddr[nIndex];
		nCurrentResult := nCurrentResult + 1;
		IF nCurrentResult > nMaxSlaveResults THEN
			CheckScannedSlaves := FALSE;
			RETURN;
		END_IF
	END_IF
END_FOR

CheckScannedSlaves := TRUE;]]></ST>
      </Implementation>
    </Method>
    <Method Name="GetNextSlaveIndex" Id="{112f5ee7-f961-4e29-b583-c6afb7f89d21}">
      <Declaration><![CDATA[METHOD PRIVATE GetNextSlaveIndex : UINT
VAR_INPUT
	nSlaveIndex : UINT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF nSlaveIndex >= ECMASTER_MAX_SLAVES THEN
	GetNextSlaveIndex := 0;
ELSE
	GetNextSlaveIndex := nSlaveIndex + 1;
END_IF]]></ST>
      </Implementation>
    </Method>
    <Method Name="GetSlaveAddresses" Id="{1d8cb1bc-e5ae-4b51-903b-b9a7a1d26623}">
      <Declaration><![CDATA[METHOD PRIVATE GetSlaveAddresses : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF NOT fbSlaveAddr.bBusy THEN
	fbSlaveAddr.bExecute := TRUE;
END_IF

fbSlaveAddr();
fbSlaveAddr.bExecute := FALSE;

IF NOT fbSlaveAddr.bBusy THEN
	IF NOT fbSlaveAddr.bError THEN
		GetSlaveAddresses := TRUE;
		RETURN;
	END_IF
END_IF

GetSlaveAddresses := FALSE;]]></ST>
      </Implementation>
    </Method>
    <Method Name="GetSlaveDiagnostics" Id="{f82a6e98-bc22-46d0-8eaa-3ea30e6de283}">
      <Declaration><![CDATA[METHOD PRIVATE GetSlaveDiagnostics
VAR_INPUT
	nSlaveIndex : REFERENCE TO UINT;
	nParameterIndex : REFERENCE TO UINT;
END_VAR
VAR
	sCoEParameter : ST_CoEParameter;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[sCoEParameter := asEtherCATSlaves[nSlaveIndex].NextCoEParameter(nParameterIndex);

IF NOT F_IsCoEParameterValid(sCoEParameter) THEN
	asEtherCATSlaves[nSlaveIndex].ProcessAlarms();
	nSlaveIndex := GetNextSlaveIndex(nSlaveIndex);
	nParameterIndex := 0;
	RETURN;
END_IF

fbCoERead.nSlaveAddr := asEtherCATSlaves[nSlaveIndex].sBasicParameters.nAddr;
fbCoERead.nIndex := sCoEParameter.nIndex;
fbCoERead.nSubIndex := sCoEParameter.nSubIndex;
fbCoERead.pDstBuf := sCoEParameter.pDstBuf;
fbCoERead.cbBufLen := sCoEParameter.cbBufLen;

IF fbCoERead.bBusy THEN
	// Still working
	fbCoERead.bExecute := TRUE;
ELSE
	// Done working
	IF fbCoERead.bExecute THEN
		fbCoERead.bExecute := FALSE;
		// Finish reading
		IF fbCoERead.bError THEN
			// Finish with error
			asEtherCATSlaves[nSlaveIndex].sBasicParameters.eCoEStatus := E_CoEStatus.Error;
			asEtherCATSlaves[nSlaveIndex].sBasicParameters.nCoEError := fbCoERead.nErrId;
		ELSE
			// Finish without error
			asEtherCATSlaves[nSlaveIndex].sBasicParameters.eCoEStatus := E_CoEStatus.OK;
		END_IF
		nParameterIndex := nParameterIndex + 1;
	ELSE
		fbCoERead.bExecute := TRUE;
	END_IF
END_IF

fbCoERead();]]></ST>
      </Implementation>
    </Method>
    <Method Name="GetSlaveIdentities" Id="{b429887b-df51-4c40-a3a0-5876f8f124ed}">
      <Declaration><![CDATA[METHOD PRIVATE GetSlaveIdentities : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF NOT fbSlaveIdentity.bBusy THEN
	fbSlaveIdentity.bExecute := TRUE;
	fbSlaveIdentity.nSlaveAddr := asEcSlaveTopology[nSlaveIndex_Identity].nOwnPhysicalAddr;
END_IF

fbSlaveIdentity();
fbSlaveIdentity.bExecute := FALSE;

IF NOT fbSlaveIdentity.bBusy AND NOT fbSlaveIdentity.bError THEN
	asEcSlaveIdentities[nSlaveIndex_Identity] := fbSlaveIdentity.identity;
	
	nSlaveIndex_Identity := nSlaveIndex_Identity + 1;
	IF nSlaveIndex_Identity > (nCfgSlaveCount - 1) THEN
		GetSlaveIdentities := TRUE;
		RETURN;
	END_IF
END_IF

GetSlaveIdentities := FALSE;]]></ST>
      </Implementation>
    </Method>
    <Method Name="GetSlavesStates" Id="{fcf07203-d4fd-4ef9-8238-77ae95349b19}">
      <Declaration><![CDATA[METHOD PRIVATE GetSlavesStates
VAR_INPUT
END_VAR
VAR
	nSlaveIndex : UINT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[fbSlaveState();

IF NOT fbSlaveState.bBusy THEN
	IF fbSlaveState.bExecute THEN
		// Done fetching
		fbSlaveState.bExecute := FALSE;
		nSlavesRead := fbSlaveState.nSlaves;
		FOR nSlaveIndex := 0 TO nSlavesRead DO
			asEtherCATSlaves[nSlaveIndex].sBasicParameters.sEtherCATState := asEcSlaveStates[nSlaveIndex];
		END_FOR
	ELSE
		fbSlaveState.bExecute := TRUE;
	END_IF
END_IF]]></ST>
      </Implementation>
    </Method>
    <Method Name="GetSlaveTopology" Id="{ac59f77c-739c-472d-bb3a-b10fa6f6b968}">
      <Declaration><![CDATA[METHOD PRIVATE GetSlaveTopology : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF NOT fbSlaveTopology.bBusy THEN
	fbSlaveTopology.bExecute := TRUE;
END_IF

fbSlaveTopology();
fbSlaveTopology.bExecute := FALSE;

IF NOT fbSlaveTopology.bBusy THEN
	IF NOT fbSlaveTopology.bError THEN
		GetSlaveTopology := TRUE;
		RETURN;
	END_IF
END_IF

GetSlaveTopology := FALSE;]]></ST>
      </Implementation>
    </Method>
    <Method Name="ScanSlaves" Id="{3ca6fa14-0e19-4e99-8b2d-f72d24da07a2}">
      <Declaration><![CDATA[METHOD PRIVATE ScanSlaves : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF NOT fbScanSlaves.bBusy THEN
	fbScanSlaves.bExecute := TRUE;
END_IF

fbScanSlaves();
fbScanSlaves.bExecute := FALSE;

IF NOT fbScanSlaves.bBusy AND NOT fbScanSlaves.bError THEN
	nScannedSlaves := fbScanSlaves.nSlaves;
	ScanSlaves := TRUE;
	RETURN;
END_IF

ScanSlaves := FALSE; ]]></ST>
      </Implementation>
    </Method>
    <Method Name="SortTopology" Id="{9ffea821-c36c-4337-8305-c3dc1dbca798}">
      <Declaration><![CDATA[METHOD PRIVATE SortTopology
VAR_INPUT
END_VAR
VAR
	nIndex : UINT;
	
	iNewEtherCATSlave : IEtherCATSlave;
	
	nDashIndex : INT;
	sNameSecondPart : STRING(4);
	sProductNameLong : STRING(64);
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[FOR nIndex := 0 TO ECMASTER_MAX_SLAVES DO
	iNewEtherCATSlave := F_EtherCATSlaveFactory(asEcSlaveIdentities[nIndex].vendorId, asEcSlaveIdentities[nIndex].productCode);
	
	asEtherCATSlaves[nIndex] := iNewEtherCATSlave;	

	asEtherCATSlaves[nIndex].sBasicParameters.nAddr := asEcSlaveTopology[nIndex].nOwnPhysicalAddr;
	asEtherCATSlaves[nIndex].sBasicParameters.sIdentity := asEcSlaveIdentities[nIndex];
	
	// Get full name
	asEtherCATSlaves[nIndex].sBasicParameters.sProductNameLong := sProductNameLong := F_ConvProductCodeToString(stSlaveIdentity := asEcSlaveIdentities[nIndex]);
	
	// Get short display name
	nDashIndex := FIND(STR1:=sProductNameLong, '-');
	
	IF nDashIndex <> 0 THEN
		sNameSecondPart := MID(STR:= sProductNameLong, POS:=nDashIndex + 1, LEN:=4);
		IF sNameSecondPart = '0000' THEN
			asEtherCATSlaves[nIndex].sBasicParameters.sProductName := LEFT(STR:= sProductNameLong, SIZE:= nDashIndex - 1);
		ELSE
			asEtherCATSlaves[nIndex].sBasicParameters.sProductName := LEFT(STR:= sProductNameLong, SIZE:= nDashIndex + 4);
		END_IF
	ELSE
		asEtherCATSlaves[nIndex].sBasicParameters.sProductName := LEFT(STR:= sProductNameLong, SIZE:= 6);
	END_IF
	
	asEtherCATSlaves[nIndex].InitAlarms();
END_FOR]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="fbECMasterMonitoring">
      <LineId Id="629" Count="62" />
      <LineId Id="44" Count="0" />
    </LineIds>
    <LineIds Name="fbECMasterMonitoring.bScanned.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="fbECMasterMonitoring.CheckScannedSlaves">
      <LineId Id="5" Count="0" />
      <LineId Id="8" Count="0" />
      <LineId Id="11" Count="1" />
      <LineId Id="20" Count="1" />
      <LineId Id="14" Count="0" />
      <LineId Id="13" Count="0" />
      <LineId Id="15" Count="0" />
      <LineId Id="17" Count="0" />
      <LineId Id="22" Count="0" />
      <LineId Id="25" Count="1" />
      <LineId Id="28" Count="0" />
      <LineId Id="27" Count="0" />
      <LineId Id="18" Count="0" />
      <LineId Id="16" Count="0" />
      <LineId Id="24" Count="0" />
      <LineId Id="23" Count="0" />
    </LineIds>
    <LineIds Name="fbECMasterMonitoring.GetNextSlaveIndex">
      <LineId Id="5" Count="0" />
      <LineId Id="7" Count="0" />
      <LineId Id="9" Count="1" />
      <LineId Id="8" Count="0" />
    </LineIds>
    <LineIds Name="fbECMasterMonitoring.GetSlaveAddresses">
      <LineId Id="5" Count="2" />
      <LineId Id="9" Count="0" />
      <LineId Id="8" Count="0" />
      <LineId Id="16" Count="0" />
      <LineId Id="11" Count="0" />
      <LineId Id="10" Count="0" />
      <LineId Id="13" Count="0" />
      <LineId Id="17" Count="0" />
      <LineId Id="21" Count="0" />
      <LineId Id="15" Count="0" />
      <LineId Id="12" Count="0" />
      <LineId Id="20" Count="0" />
      <LineId Id="19" Count="0" />
    </LineIds>
    <LineIds Name="fbECMasterMonitoring.GetSlaveDiagnostics">
      <LineId Id="28" Count="1" />
      <LineId Id="34" Count="0" />
      <LineId Id="101" Count="0" />
      <LineId Id="37" Count="0" />
      <LineId Id="64" Count="0" />
      <LineId Id="40" Count="0" />
      <LineId Id="38" Count="0" />
      <LineId Id="65" Count="1" />
      <LineId Id="68" Count="4" />
      <LineId Id="44" Count="0" />
      <LineId Id="86" Count="0" />
      <LineId Id="73" Count="0" />
      <LineId Id="75" Count="0" />
      <LineId Id="87" Count="0" />
      <LineId Id="76" Count="1" />
      <LineId Id="79" Count="2" />
      <LineId Id="89" Count="0" />
      <LineId Id="102" Count="0" />
      <LineId Id="83" Count="1" />
      <LineId Id="90" Count="0" />
      <LineId Id="82" Count="0" />
      <LineId Id="93" Count="0" />
      <LineId Id="88" Count="0" />
      <LineId Id="85" Count="0" />
      <LineId Id="78" Count="0" />
      <LineId Id="74" Count="0" />
      <LineId Id="63" Count="0" />
      <LineId Id="62" Count="0" />
    </LineIds>
    <LineIds Name="fbECMasterMonitoring.GetSlaveIdentities">
      <LineId Id="62" Count="1" />
      <LineId Id="66" Count="0" />
      <LineId Id="64" Count="1" />
      <LineId Id="16" Count="0" />
      <LineId Id="55" Count="0" />
      <LineId Id="52" Count="0" />
      <LineId Id="51" Count="0" />
      <LineId Id="53" Count="0" />
      <LineId Id="67" Count="0" />
      <LineId Id="57" Count="1" />
      <LineId Id="56" Count="0" />
      <LineId Id="73" Count="0" />
      <LineId Id="61" Count="0" />
      <LineId Id="54" Count="0" />
      <LineId Id="74" Count="0" />
      <LineId Id="20" Count="0" />
    </LineIds>
    <LineIds Name="fbECMasterMonitoring.GetSlavesStates">
      <LineId Id="5" Count="0" />
      <LineId Id="19" Count="0" />
      <LineId Id="18" Count="0" />
      <LineId Id="20" Count="0" />
      <LineId Id="22" Count="0" />
      <LineId Id="34" Count="0" />
      <LineId Id="27" Count="1" />
      <LineId Id="32" Count="1" />
      <LineId Id="24" Count="1" />
      <LineId Id="23" Count="0" />
      <LineId Id="21" Count="0" />
    </LineIds>
    <LineIds Name="fbECMasterMonitoring.GetSlaveTopology">
      <LineId Id="6" Count="9" />
      <LineId Id="17" Count="0" />
      <LineId Id="16" Count="0" />
      <LineId Id="5" Count="0" />
      <LineId Id="19" Count="0" />
      <LineId Id="18" Count="0" />
    </LineIds>
    <LineIds Name="fbECMasterMonitoring.ScanSlaves">
      <LineId Id="5" Count="2" />
      <LineId Id="9" Count="0" />
      <LineId Id="8" Count="0" />
      <LineId Id="10" Count="0" />
      <LineId Id="12" Count="0" />
      <LineId Id="11" Count="0" />
      <LineId Id="19" Count="0" />
      <LineId Id="17" Count="1" />
      <LineId Id="13" Count="0" />
      <LineId Id="16" Count="0" />
      <LineId Id="15" Count="0" />
    </LineIds>
    <LineIds Name="fbECMasterMonitoring.SortTopology">
      <LineId Id="5" Count="0" />
      <LineId Id="62" Count="1" />
      <LineId Id="9" Count="0" />
      <LineId Id="65" Count="0" />
      <LineId Id="61" Count="0" />
      <LineId Id="66" Count="0" />
      <LineId Id="17" Count="0" />
      <LineId Id="45" Count="0" />
      <LineId Id="35" Count="0" />
      <LineId Id="39" Count="0" />
      <LineId Id="46" Count="0" />
      <LineId Id="40" Count="2" />
      <LineId Id="51" Count="0" />
      <LineId Id="55" Count="0" />
      <LineId Id="43" Count="0" />
      <LineId Id="56" Count="2" />
      <LineId Id="44" Count="0" />
      <LineId Id="50" Count="0" />
      <LineId Id="49" Count="0" />
      <LineId Id="79" Count="1" />
      <LineId Id="10" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>